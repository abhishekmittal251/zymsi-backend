# Generated by Django 4.2.1 on 2023-05-21 16:31

import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('gym', '0003_alter_reviews_datetime'),
    ]

    operations = [
        migrations.CreateModel(
            name='Deals',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deal_name', models.CharField(max_length=200)),
                ('price', models.CharField(max_length=200)),
                ('discounted_price', models.CharField(max_length=200)),
                ('discount', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Timing',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type', models.CharField(choices=[('Morning', 'Morning'), ('Evening', 'Evening')], max_length=40)),
                ('opening', models.TimeField(blank=True)),
                ('closing', models.TimeField(blank=True)),
            ],
        ),
        migrations.AddField(
            model_name='gym',
            name='gym_holiday',
            field=models.CharField(default='sdf', max_length=50),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='reviews',
            name='dateTime',
            field=models.DateTimeField(default=datetime.datetime(2023, 5, 21, 22, 1, 17, 61773)),
        ),
        migrations.AddField(
            model_name='gym',
            name='gym_deals',
            field=models.ManyToManyField(to='gym.deals'),
        ),
        migrations.AddField(
            model_name='gym',
            name='gym_timing',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='gym.timing'),
            preserve_default=False,
        ),
    ]
